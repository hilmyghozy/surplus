import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:surplus/components/widget/text_widget.dart';
import 'package:surplus/screen/splash_screen/splash_screen_controller.dart';
import 'package:surplus/utils/constant.dart';

class SplashScreen extends StatelessWidget with Constant {
  SplashScreen({super.key});
  final ctrl = Get.put(SplashScreenController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        height: getHeight(context) / 1.1,
        width: getWidth(context),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset("lib/assets/logo.png", width: getWidth(context) / 2),
            const SizedBox(
              width: 180,
              child: VText(
                'Save food. Save budget. Save planet',
                align: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    );
  }
}
