import 'dart:async';
import 'package:get/get.dart';
import 'package:surplus/screen/login/login.dart';

class SplashScreenController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    timerSplashScreen();
  }

  timerSplashScreen() async {
    Timer(const Duration(seconds: 2), () {
      Get.off(LoginPage());
    });
  }
}
