import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:surplus/components/default_app_bar.dart';
import 'package:surplus/components/default_widget_container.dart';
import 'package:surplus/components/widget/text_widget.dart';
import 'package:surplus/utils/constant.dart';
import 'package:surplus/utils/v_color.dart';
import 'detail_movie_controller.dart';

// ignore: must_be_immutable
class DetailMoviePage extends StatelessWidget with Constant {
  DetailMoviePage({super.key});

  DetailMovieController ctrl = Get.put(DetailMovieController());

  @override
  Widget build(BuildContext context) {
    return DefaultWidgetContainer(
      context,
      DefaultAppBar(context, 'Detail Movie',
          showBackButton: true, roundedCorner: false),
      SingleChildScrollView(
        child: Obx(
          () => ctrl.isLoading.value == true
              ? SizedBox(
                  width: getWidth(context),
                  height: getHeight(context),
                  child: const Center(
                    widthFactor: 1,
                    child: CircularProgressIndicator(
                      color: VColor.primaryGreen,
                    ),
                  ),
                )
              : SizedBox(
                  height: getHeight(context) - 100,
                  child: Stack(
                    children: [
                      Image.network(
                        'https://image.tmdb.org/t/p/w500${ctrl.detailMovie.value.posterPath}',
                        width: getWidth(context),
                      ),
                      Positioned(
                        bottom: 0,
                        child: Container(
                          width: getWidth(context),
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: <Color>[
                                Color.fromARGB(78, 68, 194, 171),
                                Color.fromARGB(255, 68, 194, 171),
                                Color.fromARGB(255, 48, 162, 135),
                                Color.fromARGB(0, 6, 124, 116),
                              ], // Gradient from https://learnui.design/tools/gradient-generator.html
                              tileMode: TileMode.mirror,
                            ),
                          ),
                          padding: const EdgeInsets.symmetric(
                              vertical: 10, horizontal: 20),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              VText(
                                '${ctrl.detailMovie.value.title}',
                                fontSize: 30,
                                isBold: true,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 15),
                                child: VText(
                                  '${ctrl.detailMovie.value.tagline}',
                                  fontSize: 15,
                                ),
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        right: 5, bottom: 10),
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 2, horizontal: 10),
                                      decoration: BoxDecoration(
                                          color: VColor.yellow,
                                          borderRadius:
                                              BorderRadius.circular(6)),
                                      child: VText(
                                          'IMDB ${ctrl.detailMovie.value.voteAverage}'),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        right: 5, bottom: 10),
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.calendar_month,
                                          size: 15,
                                        ),
                                        VText(
                                            '  ${ctrl.detailMovie.value.releaseDate}'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              VText(
                                '${ctrl.detailMovie.value.overview}',
                                fontSize: 14,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
        ),
      ),
      liniearGradient: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: <Color>[
            VColor.primaryGreen,
            Color.fromARGB(255, 48, 162, 135),
            Color.fromARGB(255, 16, 124, 117),
          ], // Gradient from https://learnui.design/tools/gradient-generator.html
          tileMode: TileMode.mirror,
        ),
      ),
    );
  }
}
