import 'package:get/get.dart';
import 'package:surplus/models/movie_model/movie_model.dart';
import 'package:surplus/repositorys/movie_repository.dart';

class DetailMovieController extends GetxController {
  var args = Get.arguments;
  var isLoading = false.obs;

  final _movieRepo = Get.put(MovieRepository());

  final Rx<MovieModel> _detailMovie = MovieModel().obs;
  Rx<MovieModel> get detailMovie => _detailMovie;

  @override
  void onInit() {
    super.onInit();
    getMovie();
  }

  getMovie() async {
    isLoading.value = true;
    _detailMovie.value = (await _movieRepo.getDetailMovie(args))!;
    isLoading.value = false;
    update();
  }
}
