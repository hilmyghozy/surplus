import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:surplus/components/default_app_bar.dart';
import 'package:surplus/components/default_widget_container.dart';
import 'package:surplus/components/widget/text_widget.dart';
import 'package:surplus/screen/mainpage/mainpage_controller.dart';
import 'package:surplus/utils/constant.dart';
import 'package:surplus/utils/v_color.dart';

class MainPage extends StatelessWidget with Constant {
  MainPage({super.key});

  final ctrl = Get.put(MainPageController());

  @override
  Widget build(BuildContext context) {
    return DefaultWidgetContainer(
      context,
      DefaultAppBar(context, 'Movie', roundedCorner: false),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 20),
            child: Row(
              children: [
                Obx(
                  () => InkWell(
                    onTap: () {
                      ctrl.indexTab.value = 0;
                    },
                    child: Container(
                      decoration: ctrl.indexTab.value == 0
                          ? const BoxDecoration(
                              border: Border(
                                bottom:
                                    BorderSide(width: 1.5, color: VColor.white),
                              ),
                            )
                          : const BoxDecoration(),
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: VText(
                          'Now Showing',
                          fontSize: 14,
                          isBold: ctrl.indexTab.value == 0 ? true : false,
                          color: VColor.white,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 20),
                Obx(
                  () => InkWell(
                    onTap: () {
                      ctrl.indexTab.value = 1;
                    },
                    child: Container(
                      decoration: ctrl.indexTab.value == 1
                          ? const BoxDecoration(
                              border: Border(
                                bottom:
                                    BorderSide(width: 1.5, color: VColor.white),
                              ),
                            )
                          : const BoxDecoration(),
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: VText(
                          'Coming soon',
                          fontSize: 14,
                          isBold: ctrl.indexTab.value == 1 ? true : false,
                          color: VColor.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Obx(
            () => ctrl.indexTab.value == 0
                ? Expanded(
                    child: CarouselSlider(
                      options: CarouselOptions(
                        // autoPlay: true,
                        aspectRatio: 1.0,
                        enlargeCenterPage: true,
                        enlargeStrategy: CenterPageEnlargeStrategy.zoom,
                      ),
                      items: ctrl.nowShowingWidget(),
                    ),
                  )
                : Expanded(
                    child: CarouselSlider(
                      options: CarouselOptions(
                        // autoPlay: true,
                        aspectRatio: 1.0,
                        enlargeCenterPage: true,
                        enlargeStrategy: CenterPageEnlargeStrategy.zoom,
                      ),
                      items: ctrl.upComingWidget(),
                    ),
                  ),
          ),
        ],
      ),
      liniearGradient: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: <Color>[
            VColor.primaryGreen,
            Color.fromARGB(255, 48, 162, 135),
            Color.fromARGB(255, 16, 124, 117),
          ], // Gradient from https://learnui.design/tools/gradient-generator.html
          tileMode: TileMode.mirror,
        ),
      ),
    );
  }
}
