import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:surplus/components/card/movie.dart';
import 'package:surplus/models/movie_model/movie_model.dart';
import 'package:surplus/repositorys/movie_repository.dart';
import 'package:surplus/screen/detail_movie/detail_movie.dart';

class MainPageController extends GetxController {
  var isLoading = false.obs;
  var indexTab = 0.obs;

  ScrollController ctrlScroll = ScrollController();

  final _movieRepo = Get.put(MovieRepository());

  final RxList<MovieModel> _upcomingMovie = <MovieModel>[].obs;
  RxList<MovieModel> get upcomingMovie => _upcomingMovie;

  final RxList<MovieModel> _nowShowingMovie = <MovieModel>[].obs;
  RxList<MovieModel> get nowShowingMovie => _nowShowingMovie;

  @override
  void onInit() {
    super.onInit();
    getMovie(1);
    scrollListener();
  }

  getMovie(int? page) async {
    var upcomingRep = await _movieRepo.getUpcomingMovie(page);

    if (upcomingRep!.results!.isNotEmpty) {
      var data = upcomingRep.results!;
      _upcomingMovie.addAll(data);
    }

    var nowshowingRep = await _movieRepo.getNowShowingMovie(page);

    if (nowshowingRep!.results!.isNotEmpty) {
      var data = nowshowingRep.results!;
      _nowShowingMovie.addAll(data);
    }

    update();
  }

  scrollListener() {
    ctrlScroll.addListener(() {
      log('scrolling max > ${ctrlScroll.position.maxScrollExtent}| pixels ${ctrlScroll.position.pixels}');
      log('index item ${ctrlScroll.position.pixels}');
      if (ctrlScroll.position.maxScrollExtent == ctrlScroll.position.pixels) {
        log('firing');
      }
    });
  }

  nowShowingWidget() {
    final List<Widget> imageSliders = nowShowingMovie
        .map(
          (item) => InkWell(
            onTap: () {
              Get.to(() => DetailMoviePage(), arguments: item.id);
            },
            child: CardMovie(
              title: item.title.toString(),
              image: item.posterPath.toString(),
              voceCount: item.voteCount.toString(),
              voteAverage: item.voteAverage.toString(),
              popularity: item.popularity.toString(),
            ),
          ),
        )
        .toList();

    return imageSliders;
  }

  upComingWidget() {
    final List<Widget> imageSliders = upcomingMovie
        .map(
          (item) => InkWell(
            onTap: () {
              Get.to(() => DetailMoviePage(), arguments: item.id);
              log('id ${item.id}');
            },
            child: CardMovie(
              title: item.title.toString(),
              image: item.posterPath.toString(),
              voceCount: item.voteCount.toString(),
              voteAverage: item.voteAverage.toString(),
              popularity: item.popularity.toString(),
            ),
          ),
        )
        .toList();

    return imageSliders;
  }
}
