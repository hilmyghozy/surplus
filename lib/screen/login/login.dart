import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:surplus/components/default_app_bar.dart';
import 'package:surplus/components/default_widget_container.dart';
import 'package:surplus/components/widget/text_input_widget.dart';
import 'package:surplus/screen/login/login_controller.dart';
import 'package:surplus/screen/register/register.dart';
import 'package:surplus/utils/constant.dart';
import 'package:surplus/utils/v_color.dart';
import '../../components/widget/text_widget.dart';

class LoginPage extends StatelessWidget with Constant {
  LoginPage({super.key});

  final ctrl = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return DefaultWidgetContainer(
        context,
        DefaultAppBar(context, 'Login'),
        Container(
          color: VColor.primaryBackground,
          height: getHeight(context),
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const VText(
                      'Masuk',
                      fontSize: 20,
                    ),
                    const SizedBox(height: 10),
                    SizedBox(
                      width: getWidth(context) / 2,
                      child: const VText(
                          'Pastikan kamu sudah pernah membuat akun surpluss'),
                    ),
                    const SizedBox(height: 20),
                  ],
                ),
                Column(
                  children: [
                    DefaultTextInput(
                      controller: ctrl.controllerUsername,
                      hint: 'E-Mail',
                      onChanged: (p0) {
                        ctrl.checkValid();
                      },
                    ),
                    DefaultTextInput(
                      controller: ctrl.controllerPassword,
                      hint: 'Password',
                      secureText: true,
                      onChanged: (p0) {
                        ctrl.checkValid();
                      },
                    ),
                    const SizedBox(height: 40),
                    Obx(
                      () => TextButton(
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          decoration: BoxDecoration(
                              color: ctrl.isValid.value
                                  ? VColor.primaryGreen
                                  : VColor.grey2,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20))),
                          width: getWidth(context),
                          child: const VText(
                            'Login',
                            align: TextAlign.center,
                            isBold: true,
                            color: VColor.white,
                          ),
                        ),
                        onPressed: () {
                          ctrl.isValid.value
                              ? ctrl.checkAccount(context)
                              : log('cant login');
                        },
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const VText('Didnt have an account?'),
                      const SizedBox(width: 5),
                      InkWell(
                        onTap: () {
                          Get.to(() => RegisterPage());
                        },
                        child: const VText(
                          'Register',
                          color: VColor.primaryGreen,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                const VText('#notes'),
                Row(
                  children: [
                    const VText('email is: '),
                    VText(
                      ctrl.email.value,
                      fontSize: 14,
                      isBold: true,
                    )
                  ],
                ),
                Row(
                  children: [
                    const VText('password is: '),
                    VText(
                      ctrl.password.value,
                      fontSize: 14,
                      isBold: true,
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
        safeArea: true);
  }
}
