import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:surplus/screen/login/login.dart';
import 'package:surplus/screen/mainpage/mainpage.dart';

class LoginController extends GetxController {
  TextEditingController controllerUsername = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();

  var isValid = false.obs;
  var email = 'hilmy'.obs;
  var password = 'hilmy'.obs;

  @override
  void onInit() {
    super.onInit();
  }

  checkValid() {
    if (controllerUsername.value.text != '' &&
        controllerPassword.value.text != '') {
      isValid.value = true;
    } else {
      isValid.value = false;
    }
  }

  checkAccount(context) {
    if (controllerUsername.value.text == email.value &&
        controllerPassword.value.text == password.value) {
      Get.offAll(() => MainPage());
    } else {
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Login Gagal'),
          content: const Text('Email atau Password Salah'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'OK'),
              child: const Text('OK'),
            ),
          ],
        ),
      );
    }
  }

  registerAccount(context, String emailTF, String passwordTF) {
    email.value = emailTF;
    password.value = passwordTF;
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('Register Success'),
        content: const Text('Please Login for entering the apps'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Get.off(LoginPage()),
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }
}
