import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:surplus/components/default_app_bar.dart';
import 'package:surplus/components/default_widget_container.dart';
import 'package:surplus/components/widget/text_input_widget.dart';
import 'package:surplus/screen/login/login.dart';
import 'package:surplus/screen/login/login_controller.dart';
import 'package:surplus/screen/register/register_controller.dart';
import 'package:surplus/utils/constant.dart';
import 'package:surplus/utils/v_color.dart';

import '../../components/widget/text_widget.dart';
import '../mainpage/mainpage.dart';

class RegisterPage extends StatelessWidget with Constant {
  RegisterPage({super.key});

  final ctrl = Get.put(LoginController());
  final _ctrl = Get.put(RegisterPageController());

  @override
  Widget build(BuildContext context) {
    return DefaultWidgetContainer(
        context,
        DefaultAppBar(
          context,
          'Register',
          showBackButton: true,
          onBackPressed: () => Get.off(LoginPage()),
        ),
        Container(
          color: VColor.primaryBackground,
          height: getHeight(context),
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const VText(
                      'Register',
                      fontSize: 20,
                    ),
                    const SizedBox(height: 10),
                    SizedBox(
                      width: getWidth(context) / 2,
                      child: const VText(
                          'Pastikan kamu Belum pernah membuat akun surpluss'),
                    ),
                    const SizedBox(height: 20),
                  ],
                ),
                Column(
                  children: [
                    DefaultTextInput(
                      controller: _ctrl.controllerUsername,
                      hint: 'E-Mail',
                      onChanged: (p0) {
                        _ctrl.checkValid();
                      },
                    ),
                    DefaultTextInput(
                      controller: _ctrl.controllerPassword,
                      hint: 'Password',
                      secureText: true,
                      onChanged: (p0) {
                        _ctrl.checkValid();
                      },
                    ),
                    const SizedBox(height: 40),
                    Obx(
                      () => TextButton(
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          decoration: BoxDecoration(
                              color: _ctrl.isValid.value
                                  ? VColor.primaryGreen
                                  : VColor.grey2,
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20))),
                          width: getWidth(context),
                          child: const VText(
                            'Register',
                            align: TextAlign.center,
                            isBold: true,
                            color: VColor.white,
                          ),
                        ),
                        onPressed: () {
                          _ctrl.isValid.value
                              ? ctrl.registerAccount(
                                  context,
                                  _ctrl.controllerUsername.text,
                                  _ctrl.controllerPassword.text)
                              : log('cant login');
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        safeArea: true);
  }
}
