import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:surplus/components/card/movie.dart';
import 'package:surplus/models/movie_model/movie_model.dart';
import 'package:surplus/models/parent_model/parent_model.dart';
import 'package:surplus/repositorys/movie_repository.dart';
import 'package:surplus/screen/detail_movie/detail_movie.dart';
import 'package:surplus/screen/detail_movie/detail_movie_controller.dart';
import 'package:surplus/screen/mainpage/mainpage.dart';

class RegisterPageController extends GetxController {
  TextEditingController controllerUsername = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();

  var isValid = false.obs;
  var email = 'hilmy'.obs;
  var password = 'hilmy'.obs;

  @override
  void onInit() {
    super.onInit();
  }

  checkValid() {
    if (controllerUsername.value.text != '' &&
        controllerPassword.value.text != '') {
      isValid.value = true;
    } else {
      isValid.value = false;
    }
  }

  checkAccount(context) {
    if (controllerUsername.value.text == email.value &&
        controllerPassword.value.text == password.value) {
      Get.offAll(() => MainPage());
    } else {
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text('Login Gagal'),
          content: const Text('Email atau Password Salah'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'OK'),
              child: const Text('OK'),
            ),
          ],
        ),
      );
    }
  }
}
