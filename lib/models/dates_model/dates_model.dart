import 'package:json_annotation/json_annotation.dart';
part 'dates_model.g.dart';

@JsonSerializable()
class DatesModel {
  DatesModel();

  String? maximum;
  String? minimum;

  factory DatesModel.fromJson(Map<String, dynamic> json) =>
      _$DatesModelFromJson(json);
}
