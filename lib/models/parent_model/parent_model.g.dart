// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'parent_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ParentModel _$ParentModelFromJson(Map<String, dynamic> json) => ParentModel()
  ..dates = json['dates'] == null
      ? null
      : DatesModel.fromJson(json['dates'] as Map<String, dynamic>)
  ..page = json['page'] as int?
  ..results = (json['results'] as List<dynamic>?)
      ?.map((e) => MovieModel.fromJson(e as Map<String, dynamic>))
      .toList()
  ..totalPages = json['totalPages'] as int?
  ..totalResults = json['totalResults'] as int?;

Map<String, dynamic> _$ParentModelToJson(ParentModel instance) =>
    <String, dynamic>{
      'dates': instance.dates,
      'page': instance.page,
      'results': instance.results,
      'totalPages': instance.totalPages,
      'totalResults': instance.totalResults,
    };
