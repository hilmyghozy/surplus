import 'package:json_annotation/json_annotation.dart';
import 'package:surplus/models/dates_model/dates_model.dart';
import 'package:surplus/models/movie_model/movie_model.dart';
part 'parent_model.g.dart';

@JsonSerializable()
class ParentModel {
  ParentModel();

  DatesModel? dates;
  int? page;
  List<MovieModel>? results;
  int? totalPages;
  int? totalResults;

  factory ParentModel.fromJson(Map<String, dynamic> json) =>
      _$ParentModelFromJson(json);
}
