// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MovieModel _$MovieModelFromJson(Map<String, dynamic> json) => MovieModel()
  ..adult = json['adult'] as bool?
  ..backdropPath = json['backdrop_path'] as String?
  ..genreIds =
      (json['genre_ids'] as List<dynamic>?)?.map((e) => e as int).toList()
  ..id = json['id'] as int?
  ..originalLanguage = json['original_language'] as String?
  ..originalTitle = json['original_title'] as String?
  ..overview = json['overview'] as String?
  ..popularity = (json['popularity'] as num?)?.toDouble()
  ..posterPath = json['poster_path'] as String?
  ..releaseDate = json['release_date'] as String?
  ..title = json['title'] as String?
  ..tagline = json['tagline'] as String?
  ..video = json['video'] as bool?
  ..voteAverage = (json['vote_average'] as num?)?.toDouble()
  ..voteCount = json['vote_count'] as int?;

Map<String, dynamic> _$MovieModelToJson(MovieModel instance) =>
    <String, dynamic>{
      'adult': instance.adult,
      'backdropPath': instance.backdropPath,
      'genreIds': instance.genreIds,
      'id': instance.id,
      'originalLanguage': instance.originalLanguage,
      'originalTitle': instance.originalTitle,
      'overview': instance.overview,
      'popularity': instance.popularity,
      'posterPath': instance.posterPath,
      'releaseDate': instance.releaseDate,
      'title': instance.title,
      'tagline': instance.tagline,
      'video': instance.video,
      'voteAverage': instance.voteAverage,
      'voteCount': instance.voteCount,
    };
