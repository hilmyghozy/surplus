import 'package:json_annotation/json_annotation.dart';
part 'collection_model.g.dart';

@JsonSerializable()
class CollectionModel {
  CollectionModel();

  int? id;
  String? name;
  String? posterPath;
  String? backdropPath;

  factory CollectionModel.fromJson(Map<String, dynamic> json) =>
      _$CollectionModelFromJson(json);
}
