import 'package:json_annotation/json_annotation.dart';
part 'production_countries_model.g.dart';

@JsonSerializable()
class ProductionCountriesModel {
  ProductionCountriesModel();

  int? id;
  String? logoPath;
  String? name;
  String? originCountry;

  factory ProductionCountriesModel.fromJson(Map<String, dynamic> json) =>
      _$ProductionCountriesModelFromJson(json);
}
