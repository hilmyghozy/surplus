// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'production_companies_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductionCompaniesModel _$ProductionCompaniesModelFromJson(
        Map<String, dynamic> json) =>
    ProductionCompaniesModel()
      ..id = json['id'] as int?
      ..logoPath = json['logo_path'] as String?
      ..name = json['name'] as String?
      ..originCountry = json['origin_country'] as String?;

Map<String, dynamic> _$ProductionCompaniesModelToJson(
        ProductionCompaniesModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'logoPath': instance.logoPath,
      'name': instance.name,
      'originCountry': instance.originCountry,
    };
