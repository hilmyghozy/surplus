import 'package:json_annotation/json_annotation.dart';
part 'production_companies_model.g.dart';

@JsonSerializable()
class ProductionCompaniesModel {
  ProductionCompaniesModel();

  int? id;
  String? logoPath;
  String? name;
  String? originCountry;

  factory ProductionCompaniesModel.fromJson(Map<String, dynamic> json) =>
      _$ProductionCompaniesModelFromJson(json);
}
