// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'spoken_language_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SpokenLanguagesModel _$SpokenLanguagesModelFromJson(
        Map<String, dynamic> json) =>
    SpokenLanguagesModel()
      ..englishName = json['english_name'] as String?
      ..iso6391 = json['iso_639_1'] as String?
      ..name = json['name'] as String?;

Map<String, dynamic> _$SpokenLanguagesModelToJson(
        SpokenLanguagesModel instance) =>
    <String, dynamic>{
      'englishName': instance.englishName,
      'iso6391': instance.iso6391,
      'name': instance.name,
    };
