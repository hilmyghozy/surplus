import 'package:json_annotation/json_annotation.dart';
part 'genres_model.g.dart';

@JsonSerializable()
class GenresModel {
  GenresModel();

  int? id;
  String? name;

  factory GenresModel.fromJson(Map<String, dynamic> json) =>
      _$GenresModelFromJson(json);
}
