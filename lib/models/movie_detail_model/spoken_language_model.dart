import 'package:json_annotation/json_annotation.dart';
part 'spoken_language_model.g.dart';

@JsonSerializable()
class SpokenLanguagesModel {
  SpokenLanguagesModel();

  String? englishName;
  String? iso6391;
  String? name;

  factory SpokenLanguagesModel.fromJson(Map<String, dynamic> json) =>
      _$SpokenLanguagesModelFromJson(json);
}
