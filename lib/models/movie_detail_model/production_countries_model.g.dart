// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'production_countries_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductionCountriesModel _$ProductionCountriesModelFromJson(
        Map<String, dynamic> json) =>
    ProductionCountriesModel()
      ..id = json['id'] as int?
      ..logoPath = json['logoPath'] as String?
      ..name = json['name'] as String?
      ..originCountry = json['originCountry'] as String?;

Map<String, dynamic> _$ProductionCountriesModelToJson(
        ProductionCountriesModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'logoPath': instance.logoPath,
      'name': instance.name,
      'originCountry': instance.originCountry,
    };
