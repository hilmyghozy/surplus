import 'package:json_annotation/json_annotation.dart';
import 'package:surplus/models/movie_detail_model/production_companies_model.dart';
import 'package:surplus/models/movie_detail_model/collection_model.dart';
import 'package:surplus/models/movie_detail_model/genres_model.dart';
import 'package:surplus/models/movie_detail_model/production_countries_model.dart';
import 'package:surplus/models/movie_detail_model/spoken_language_model.dart';
part 'movie_detail_model.g.dart';

@JsonSerializable()
class MovieDetailModel {
  MovieDetailModel();

  bool? adult;
  String? backdropPath;
  CollectionModel? belongsToCollection;
  int? budget;
  List<GenresModel>? genres;
  String? homepage;
  int? id;
  String? imdbId;
  String? originalLanguage;
  String? originalTitle;
  String? overview;
  double? popularity;
  String? posterPath;
  List<ProductionCompaniesModel>? productionCompanies;
  List<ProductionCountriesModel>? productionCountries;
  String? releaseDate;
  int? revenue;
  int? runtime;
  List<SpokenLanguagesModel>? spokenLanguages;
  String? status;
  String? tagline;
  String? title;
  bool? video;
  double? voteAverage;
  int? voteCount;

  factory MovieDetailModel.fromJson(Map<String, dynamic> json) =>
      _$MovieDetailModelFromJson(json);
}
