import 'package:surplus/models/movie_model/movie_model.dart';
import 'package:surplus/models/parent_model/parent_model.dart';
import 'package:surplus/services/network/api_endpoints.dart';
import 'package:surplus/services/network/base_repo.dart';
import 'package:surplus/services/network/base_result.dart';

class MovieRepository extends BaseRepo {
  MovieRepository();

  Future<ParentModel?> getUpcomingMovie(int? page) async {
    var params = {
      "api_key": Endpoint.apiKey,
      "language": "en-US",
      "page": page ?? 1,
    };

    BaseResult response =
        await get(Endpoint.movieUpComing, queryParameters: params);
    switch (response.status) {
      case ResponseStatus.success:
        try {
          var responseData = ParentModel.fromJson(response.data);
          return (responseData);
        } catch (e) {
          return null;
        }
      default:
        break;
    }
    return null;
  }

  Future<ParentModel?> getNowShowingMovie(int? page) async {
    var params = {
      "api_key": Endpoint.apiKey,
      "language": "en-US",
      "page": page ?? 1,
    };

    BaseResult response =
        await get(Endpoint.movieNowPlaying, queryParameters: params);
    switch (response.status) {
      case ResponseStatus.success:
        try {
          var responseData = ParentModel.fromJson(response.data);
          return (responseData);
        } catch (e) {
          return null;
        }
      default:
        break;
    }
    return null;
  }

  Future<MovieModel?> getDetailMovie(int? id) async {
    var params = {
      "api_key": Endpoint.apiKey,
      "language": "en-US",
    };

    BaseResult response =
        await get('${Endpoint.movie}/$id', queryParameters: params);
    switch (response.status) {
      case ResponseStatus.success:
        try {
          var responseData = MovieModel.fromJson(response.data);
          return (responseData);
        } catch (e) {
          return null;
        }
      default:
        break;
    }
    return null;
  }
}
