import 'package:flutter/material.dart';

class VColor {
  ///UNIVERSAL#, 100%
  ///background: #;

  static const Color primaryBackground = Color(0XFFF2F2F2);
  static const Color primaryContainer = Color(0xFFF1F8F9);

  static const Color textBlack = Color(0xFF000000);
  static const Color textGreen = Color(0xFF44C2AB);
  static const Color textWhite = Color(0XFFF3F5F9);

  static const Color buttonBlack = Color(0XFF1F1F21);
  static const Color buttonWhite = Color(0XFFF3F5F9);
  static const Color buttonGrey = Color(0xFFCBCBCB);

  static const Color hintText = Color(0XFFA0AAB8);
  static const Color backgroundGrey = Color(0XFFF3F5F9);
  static const Color dividerGrey = Color(0XFFE0E5ED);

  static const Color green = Color(0XFF00AC56);
  static const Color white = Colors.white;
  static const Color blue = Color(0XFFA3D4F0);
  static const Color yellow = Color(0xFFFFD817);
  static const Color red = Color(0XFFEB4040);
  static const Color pink = Color(0XFFDE2072);

  static const Color primaryGreen = Color(0xFF44C2AB);
  static const Color mediumGreen = Color(0xFFD8F1EE);

  static const Color primaryGold = Color(0xFFBBAB7F);
  static const Color lightGold = Color(0xFF979F9E);

  static const Color primarygrey = Color(0xFF979F9E);
  static const Color grey = Color(0XFF747474);
  static const Color grey1 = Color(0xFFF1F1F1);
  static const Color grey2 = Color(0xFF9295A1);
  static const Color grey3 = Color(0xFFEEEEEE);
}
