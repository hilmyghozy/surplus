import 'package:flutter/material.dart';

class Constant {
  getHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  getWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }
}
