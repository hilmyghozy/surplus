import 'package:surplus/utils/v_color.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// ignore: non_constant_identifier_names
AppBar DefaultAppBar(
  context,
  String? title, {
  dynamic action,
  VoidCallback? onBackPressed,
  bool showBackButton = false,
  bool showSearchBar = false,
  String? searchbarHint,
  bool roundedCorner = true,
  Function()? searchTap,
  TextEditingController? searchController,
  Function(String)? searchOnChange,
  Function()? searchSuffixAction,
}) {
  return AppBar(
      toolbarHeight: 56,
      centerTitle: false,
      titleSpacing: 0,
      shape: roundedCorner
          ? const RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(14),
              ),
            )
          : null,
      surfaceTintColor: VColor.textBlack,
      leading: showBackButton
          ? IconButton(
              icon: const Icon(
                Icons.arrow_back_ios,
                color: VColor.white,
                size: 15,
              ),
              onPressed: onBackPressed ??
                  () {
                    Get.back();
                  })
          : null,
      iconTheme: const IconThemeData(
          color: VColor.primaryGold, //change your color here
          size: 18),
      backgroundColor: VColor.primaryGreen,
      elevation: 0.0,
      title: Container(
        padding: showBackButton
            ? const EdgeInsets.all(0)
            : const EdgeInsets.symmetric(horizontal: 16),
        child: Text(
          title ?? "",
          style: const TextStyle(
              fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
          textAlign: TextAlign.center,
        ),
      ),
      actions: [action ?? Container()]);
}
