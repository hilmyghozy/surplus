import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:surplus/utils/v_color.dart';

class VText extends StatelessWidget {
  final dynamic title;
  final double fontSize;
  final FontWeight? fontWeight;
  final TextOverflow? overflow;
  final TextAlign? align;
  final bool money;
  final bool number;
  final bool isItalic;
  final bool isBold;
  final bool isHeadline;
  final TextDecoration? decoration;
  final int? maxLines;
  final Color color;
  final bool footer;
  final double? spacing;

  const VText(
    this.title, {
    Key? key,
    this.fontSize = 12,
    this.fontWeight,
    this.overflow,
    this.money = false,
    this.number = false,
    this.decoration,
    this.maxLines,
    this.align,
    this.isBold = false,
    this.isItalic = false,
    this.isHeadline = false,
    this.color = VColor.textBlack,
    this.footer = false,
    this.spacing = 0.5,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title.toString(),
      style: GoogleFonts.plusJakartaSans(
          color: footer ? VColor.grey2 : color,
          fontSize: fontSize,
          fontWeight: isBold ? FontWeight.bold : fontWeight ?? FontWeight.w500,
          decoration: decoration,
          decorationThickness: 4,
          letterSpacing: spacing,
          fontStyle: isItalic ? FontStyle.italic : FontStyle.normal),
      overflow: overflow ?? TextOverflow.clip,
      textAlign: align ?? TextAlign.start,
      maxLines: maxLines,
    );
  }
}
