import 'package:surplus/utils/constant.dart';
import 'package:surplus/utils/v_color.dart';
import 'package:flutter/material.dart';

class DefaultTextInput extends StatefulWidget {
  final String? hint;
  final InputDecoration? decoration;
  final TextEditingController? controller;
  final bool? secureText;
  final Function(String)? onChanged;
  final Function()? toggleBtn;
  final TextInputType? keyboardType;
  final EdgeInsets? padding;
  final double? width;
  final TextStyle? style;
  final String? errorText;
  final FocusNode? focusNode;
  final bool readOnly;
  final bool autocorrect;

  const DefaultTextInput(
      {Key? key,
      this.hint,
      this.decoration,
      this.controller,
      this.secureText,
      this.onChanged,
      this.keyboardType,
      this.padding,
      this.style,
      this.width,
      this.errorText,
      this.focusNode,
      this.toggleBtn,
      this.readOnly = false,
      this.autocorrect = false})
      : super(key: key);

  @override
  _DefaultTextInput createState() => _DefaultTextInput();
}

class _DefaultTextInput extends State<DefaultTextInput> with Constant {
  final textFieldFocusNode = FocusNode();
  bool hidePassword = true;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // padding: padding ?? const EdgeInsets.only(top: 10, left: 10, right: 10),
      width: widget.width ?? getWidth(context),
      child: Padding(
        padding: widget.padding ?? const EdgeInsets.symmetric(vertical: 10),
        child: TextField(
          readOnly: widget.readOnly,
          autocorrect: widget.autocorrect,
          focusNode: widget.focusNode,
          controller: widget.controller,
          obscureText: widget.secureText == true ? hidePassword : false,
          onChanged: widget.onChanged,
          keyboardType: widget.keyboardType,
          cursorColor: VColor.grey,
          cursorWidth: 1,
          style: TextStyle(
            color: widget.readOnly ? VColor.grey2 : VColor.textBlack,
          ),
          decoration: InputDecoration(
            labelText: widget.hint,
            labelStyle: widget.style ??
                const TextStyle(
                  color: VColor.grey,
                  fontSize: 14,
                ),
            enabledBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: VColor.buttonGrey),
            ),
            focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(width: 1, color: VColor.primaryGreen),
            ),
            errorBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: VColor.red),
            ),
            errorText: widget.errorText,
            suffixIcon: widget.secureText == true
                ? Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 4, 0),
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          if (widget.secureText == true) {
                            hidePassword = !hidePassword;
                            if (textFieldFocusNode.hasPrimaryFocus) return;

                            textFieldFocusNode.canRequestFocus = false;
                          }
                        });
                      },
                      child: Icon(
                        hidePassword == true
                            ? Icons.visibility_rounded
                            : Icons.visibility_off_rounded,
                        size: 20,
                        color: VColor.grey,
                      ),
                    ),
                  )
                : null,
          ),
        ),
      ),
    );
  }
}
