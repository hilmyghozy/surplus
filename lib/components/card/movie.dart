import 'package:flutter/material.dart';
import 'package:surplus/components/widget/text_widget.dart';
import 'package:surplus/utils/constant.dart';
import 'package:surplus/utils/v_color.dart';

class CardMovie extends StatelessWidget with Constant {
  final String title;
  final String image;
  final String? date;
  final String voceCount;
  final String voteAverage;
  final String popularity;

  const CardMovie({
    super.key,
    required this.title,
    required this.image,
    this.date,
    required this.voceCount,
    required this.voteAverage,
    required this.popularity,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getWidth(context),
      margin: const EdgeInsets.all(5),
      child: Column(
        children: [
          Image.network(
            'https://image.tmdb.org/t/p/w500$image',
            width: getWidth(context) / 1.2,
            fit: BoxFit.cover,
          ),
          date != null
              ? Padding(
                  padding: const EdgeInsets.only(
                    top: 10,
                  ),
                  child: VText(
                    date ?? '',
                    align: TextAlign.center,
                    fontSize: 17,
                    isBold: true,
                    color: VColor.white,
                  ),
                )
              : Container(),
          Padding(
            padding: const EdgeInsets.only(top: 15, bottom: 20),
            child: VText(
              title,
              isBold: true,
              align: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              fontSize: 20,
              color: VColor.white,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  VText(
                    voceCount,
                    fontSize: 17,
                    fontWeight: FontWeight.w600,
                    color: VColor.white,
                  ),
                  const SizedBox(height: 5),
                  const VText(
                    'Total Vote',
                    color: VColor.grey1,
                  ),
                ],
              ),
              Column(
                children: [
                  VText(
                    voteAverage,
                    fontSize: 17,
                    fontWeight: FontWeight.w600,
                    color: VColor.white,
                  ),
                  const SizedBox(height: 5),
                  const VText(
                    'IMDB',
                    color: VColor.grey1,
                  ),
                ],
              ),
              Column(
                children: [
                  VText(
                    popularity,
                    fontSize: 17,
                    fontWeight: FontWeight.w600,
                    color: VColor.white,
                  ),
                  const SizedBox(height: 5),
                  const VText(
                    'Popularity',
                    color: VColor.grey1,
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
