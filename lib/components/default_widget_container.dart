import 'package:surplus/utils/v_color.dart';
import 'package:flutter/material.dart';

// ignore: non_constant_identifier_names
Scaffold DefaultWidgetContainer(
  context,
  PreferredSizeWidget appBar,
  Widget widget, {
  bool safeArea = false,
  Color color = VColor.primaryBackground,
  BoxDecoration liniearGradient = const BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: <Color>[
        Color.fromARGB(181, 255, 255, 255),
        Color.fromARGB(159, 255, 255, 255),
        VColor.mediumGreen,
        VColor.primaryGreen,
      ], // Gradient from https://learnui.design/tools/gradient-generator.html
      tileMode: TileMode.mirror,
    ),
  ),
}) {
  return Scaffold(
    appBar: appBar,
    body: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: !safeArea
            ? Container(
                decoration: liniearGradient,
                child: widget,
              )
            : Container(
                color: color,
                child: SafeArea(
                  child: Container(
                    color: color,
                    child: widget,
                  ),
                ),
              )),
  );
}
