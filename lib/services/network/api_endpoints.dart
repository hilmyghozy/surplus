class Endpoint {
  static const String apiKey = "243caada71bf2377ce0f3715bd48bc26";
  static const String baseUrl = "https://api.themoviedb.org/3/";
  static const String movieNowPlaying = "movie/now_playing";
  static const String movieUpComing = "movie/upcoming";
  static const String movie = "movie/";
}
